# Дополнительно
## Метод add_field()
Этот метод позволяет изменять уже созданную в базе данных таблицу, 
а именно добавлять новое поле.<br>
На вход принимает 1 обязательный аргумент: 
`field_name` — название нового поля. 
А так же аргумент `start_value` — значение, 
которое станет значением по умолчанию. 
Если значение по умолчанию указано в описании таблицы, 
то указывать его здесь не обязательно.

!!! example "Пример добавления нового поля"
    В общем виде
    ```python
    table.add_field(field_name='new_field', start_value=value)
    ```

    Для таблицы `Points`
    ```python
    points.add_field('attribs', start_value=List([]))
    ```

!!! note "Примечание"
    Прежде чем добавить поле в таблицу, 
    нужно добавить его в описание таблицы.
