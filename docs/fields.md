# Поля и типы данных

## Типы данных
Прежде чем мы продолжим модифицировать наш класс `Points`, 
углубимся в техническую часть и разберёмся, 
как Sqlite3 API хранит данные.

### Стандартные типы данных
К стандартным типам данных относятся `str` и `int`,
которые мы уже использовали при описании полей в таблице `Points`.
Этих всем известных типов данных нет в синтаксисе Sqlite, 
поэтому API переводит привычный для питонистов `str` 
в привычный для Sqlite `TEXT`, а `int` в `INTEGER` 
и обратно соответственно.

### Пользовательские типы данных
Известно, что в Sqlite есть 4 основных типа данных, 
это: `INTEGER`, `TEXT`, `BLOB` и `REAL`.<br>
Поэтому, чтобы хранить например списки и словари, 
были реализованы `пользовательские типы данных`.
Они находятся в файле [`tupes.py`](https://github.com/AlexDev-py/sqlite3_api/blob/2.0.0/types.py).

!!! example "Пример использования пользовательских типов данных"
    ```python
    from sqlite3_api.Table import Table
    from sqlite3_api.field_types import List


    class Students(Table):
        first_name: str
        last_name: str
        age: int
        marks: List
    ```
    
    В данном случае поле `marks` будет представлено в виде списка, 
    а так же будет иметь все свойства и 
    методы стандартного для питона типа данных `list`

### Таблица типов данных

| Тип данных    | str   | int     | List  | Dict  |
| :-------      | :---: | :---:   | :---: | :---: |
| Расшифровка   | TEXT  | INTEGER | list  | dict  |

### Создание пользовательских типов данных
Умение создавать пользовательские типы данных — полезное умение, 
так как пользовательские типы данных значительно облегчают 
хранение и использование данных, тип которых не поддерживает Sqlite.

Давайте научимся на примере.<br>
Создадим тип данных, который будет хранить координаты на двумерной плоскости.

```python
from sqlite3_api.field_types import FieldType


class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))
```

Разберёмся как мы это сделали.

------
Импортируем инструмент для создания своих типов данных.
```python hl_lines="1"
from sqlite3_api.field_types import FieldType
```

------
Создаем класс, родителем которого является `FieldType`.
```python hl_lines="1"
class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))
```

------
Добавляем конструктор класса и определяем атрибуты `x` и `y` — координаты.
```python hl_lines="2 3 4"
class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))
```

------
Определяем метод `adapter`, который будет переводить наш тип данных в понятый для Sqlite.

!!! info ""
    По умолчанию `adapter` выглядит так.
    ```python
    @staticmethod
    def adapter(obj) -> bytes:
        return str(obj).encode('ascii')
    ```

Параметр `obj` — объект нашего типа данных.<br>
В нашем случае — объект класса `Position`.

```python hl_lines="6 7 8"
class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))
```

------
Определяем метод `converter`, который будет переводить то, что находилось в базе данных, в наш тип данных.

!!! warning ""
    * По умолчанию `converter` не определён. Его необходимо определить самостоятельно.<br>
    * Метод должен возвращать объект нашего класса.

Параметр `obj` — то же самое, что возвращает метод `adapter`.

```python hl_lines="10 11"
class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))
```

------

!!! example "Ещё один пример"
    Создадим тип данных, который будет хранить 
    числа с плавающий точкой(`float`), 
    который почему-то не был реализован в v2.0.0.

    ```python
    class Float(FieldType, float):
        def converter(self, obj: bytes):
            return Float(obj)
    ```

    * Здесь мы не определяем метод `__init__`, 
        так как наш тип данных наследуется от `float`.
    * Метод `adapter` мы не определяем, 
        так как его состояние по умолчанию подходит под наши цели.

## Модификация класса `Points`
Мы не зря писали новый тип данных `Position`. 
Он поможет нам хранить координаты точек в таблице `Points`.

Теперь наш код выглядит так

```python
from sqlite3_api.field_types import FieldType
from sqlite3_api import Table


class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))


class Points(Table):
    position: Position
    color: str
    size: int
```

## Значения по умолчанию
Допустим мы пишем игру и нам необходимо хранить данные об игроках.<br>
Опишем таблицу `Players`, которая будет хранить их данные.

```python
from sqlite3_api import Table
from sqlite3_api.field_types import List


class Players(Table):
    nick: str
    gender: str
    lvl: int
    hp: int
    score: int
    items: List
```

В таком случае, при регистрации новых игроков, 
мы должны будем указывать значение каждого поля.
(ведь мы не можем оставить их пустыми)
(может быть расписать 6 значений не так сложно, 
но представим что их намного больше).<br>
Именно в таких случаях нам помогут значения по умолчанию. 

Вот как будет выглядеть код

```python
class Players(Table):
    nick: str
    gender: str
    lvl: int = 1
    hp: int = 100
    score: int = 0
    items: List = List([])
```

Теперь при регистрации игроков, нам нужно будет указать всего лишь `nick` и `gender`.


!!! warning "Предупреждение"
    При обозначении полей по умолчанию 
    необходимо указывать значения с тем же типом данных!<br>
    Обратимся к полю `items` из таблицы `Players`. 
    Значением по умолчанию для этого поля мы указали `List([])`.

## Модификация класса `Points` №2
Укажем значения по умолчанию для некоторых полей нашей таблицы.<br>
Тогда наш код станет таким

```python
from sqlite3_api.field_types import FieldType
from sqlite3_api import Table


class Position(FieldType):
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    @staticmethod
    def adapter(obj) -> bytes:
        return ('%f;%f' % (obj.x, obj.y)).encode('ascii')

    def converter(self, obj: bytes):
        return Position(*map(float, obj.split(b";")))


class Points(Table):
    position: Position
    color: str = 'black'
    size: int = 1
```

!!! note "Примечание"
    Если бы мы хотели указать значение по умолчанию для поля `position`.<br>
    Мы бы сделали так:
    ```python
    position: Position = Position(x=0, y=0)
    ```

-------

Наше описание таблицы готово! Можем переходить к работе с ней!
