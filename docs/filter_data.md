# Чтение данных
Хорошо что мы научились добавлять записи в таблицу, 
но как нам узнать что мы туда сохранили? В этом нам поможет метод `filter()`.<br>
Посмотрим как он работает и обобщим его функционал в конце.

## Вывод всех записей
Напишем код, который выведет все записи в таблице.

```python hl_lines="11 12 13"
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(position=Position(4, 5.5))
points.insert(position=Position(5, 6.5))
points.insert(position=Position(0, 0), color='red')
points.insert(position=Position(-1, 1.2), size=2)
points.insert(position=Position(2, -2), color='blue', size=3)

data = points.filter()
for obj in data:
    print(obj, '\n')
```

Именно выделенный код отвечает за получение и вывод всех записей из таблицы.
Остальной код мы взяли из пред идущего раздела о добавлении данных. 
(В следующих примерах мы будем опускать эту часть кода)

## Фильтрация

У нас получилось вывести все записи, но давайте научимся их фильтровать.

Попробуем вывести только черные точки.

```python
for obj in points.filter(color='black'):
    print(obj, '\n')
```

Это оказалось не так уж сложно.

### Постфиксы

Ну а что если мы захотим вывести все точки кроме черных?<br>
Мы ведь не можем написать `filter(color!='black')`. 
В Sqlite3 API это реализовано с помощью постфиксов.
Например для решения поставленной нами задачи используется постфикс `no`.
Вот как это выглядит:

```python hl_lines="1"
for obj in points.filter(color_no='black'):
    print(obj, '\n')
```

Мы просто добавили нужный нам постфикс к названию поля, **разделяя их нижним подчеркиванием**.

!!! note "Примечание"
    Наш код не сломается, даже есть в названии поля уже присутствует нижнее подчеркивание.
    ```python
    filter(first_filed_no=val)
    ```

В Sqlite3 API реализовано несколько постфиксов:



| Постфикс    | gt    | lt    | no    | egt   | elt   |
| :-------    | :---: | :---: | :---: | :---: | :---: |
| Расшифровка | >     | <     | !=    | >=    | <=    |

!!! example "Примеры использования постфиксов"
    Давайте выведем все не черные точки размер которых меньше двух.
    ```python
    for obj in points.filter(color_no='black', size_lt=2, return_list=True):
        print(obj, '\n')
    ```

    Выведем точки которые находятся на нулевых координатах.
    ```python
    for obj in points.filter(position=Position(0, 0), return_list=True):
        print(obj, '\n')
    ```

## Параметр **return_list**
Факт: Метод `filter` может вернуть объект, а может список объектов
в зависимости от их количества полученного при фильтрации.
Поэтому все наши пред идущие примеры могут вызывать исключения.
Что бы такого не было нам поможет аргумент `return_list`. 
Если указать для него значение `True`, то метод `filter` будет возвращать список, 
даже если в таблице нет записей подходящих условию.

К примеру получим список точек размер которых равен 2.

```python
for obj in points.filter(return_list=True, size=2):
    print(obj, '\n')
```

!!! note "Что если бы мы не указали `return_list`"
    ```python
    for obj in points.filter(size=2):
        print(obj, '\n')
    ```
    
    Данный код вызовет исключение `TypeError`

## Параметр **return_type**
Этот параметр может принимать 2 значения:
`classes` или `visual`

### classes
По умолчанию параметр `return_type` принимает именно это значение.
Это означает что метод `filter` возвращает объекты нашего описания.

!!! example "Проведём эксперимент"
    Запустим такой код:
    ```python
    for obj in points.filter():
        print(type(obj))
    ```

    На каждой строке будет выведено чтото похожее на это: `<class 'data_base.Points'>`.
    Это означает что для каждого `obj` верно утверждение `isinstance(obj, Points)`

### visual
Если указать это значение, то метод `filter` 
вернёт обычный массив данных, 
как это делает `fetchall` из модуля sqlite3.

!!! example "Проведём эксперимент"
    Запустим такой код:
    ```python
    for obj in points.filter(return_type='visual'):
        print(type(obj))
    ```

    Код будет выводить `<class 'tuple'>`.

## Метод filter()
Метод, возвращающий записи из таблицы, 
подходящие определённым условиям.<br>
Имеет 2 параметра по умолчанию, это `return_list` и `return_type`.<br>
Так же может принимать параметры фильтрации.

!!! example "Пример фильтрации записей"
    ```python
    conditions = dict(first_filed_no=1, second_filed=1)
    data = table.filter(return_list=True, **conditions)
    ```
