# Welcome to Sqlite3 API

Sqlite3 API позволяет легко оперировать базами данных Sqlite3, используя ООП

!!! info ""
    Данная документация актуальна для версии 2.0.1

---------------------------------------------

## Пример программы
```python linenums="1"
from sqlite3_api.Table import Table


class Users(Table):
    first_name: str
    last_name: int
    age: int


my_table = Users(db_path=':memory:')
# Создаём таблицу
my_table.create_table()

# Заполняем таблицу
my_table.insert(first_name='Ян', last_name='Иванов', age=18)
my_table.insert(first_name='Павел', last_name='Петров', age=18)
my_table.insert(first_name='Иван', last_name='Петров', age=20)

# Получаем все данные из таблицы
for obj in my_table.filter():
    print(obj, '\n')
print('-' * 50)

# Получаем пользователей, которым 18 лет
for obj in my_table.filter(age=18):
    print(obj, '\n')
print('-' * 50)

# Получаем первого пользователя
obj = my_table.filter(id=1)
# Выведем его данные
print(f'{obj.first_name=}')
print(f'{obj.last_name=}')
print(f'{obj.age=}')

# Изменим его возраст
obj.age += 1
print(f'{obj.age=}')
# Сохраним
print(f'{obj.save()=}')
```