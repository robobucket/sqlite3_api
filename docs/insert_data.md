# Добавление данных

Предположим мы создали базу данных, но возникает проблема...<br>
Что бы работать с базой данных, нужно чтобы в ней что-то было!<br>
Поэтому самое время познакомиться с новым методом!

## метод insert()
Метод, добавляющий в таблицу новую запись.<br>
В качестве параметров принимает названия полей и их значения.

!!! example "Пример добавления записи"
    ```python
    table.insert(field1=value1, field2=value2)
    ```

Перейдем к нашей таблице `Points` и добавим в неё пару точек.

### Код v1

!!! note ""
    Мы будем модифицировать этот код.

```python
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(
    position=Position(4, 5.5), color='black', size=1
)
points.insert(
    position=Position(5, 6.5), color='black', size=1
)
```

Разберёмся что произошло.

------
Импортируем описание таблицы и новый тип данных, 
создаем соединение описания с базой данных, 
а так же создаем таблицу.

```python hl_lines="1 2 3"
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(
    position=Position(4, 5.5), color='black', size=1
)
points.insert(
    position=Position(5, 6.5), color='black', size=1
)
```

!!! note "Примечание"
    Создание таблицы в базе данных необходимо в нашем случае, 
    так как мы храним базу данных в оперативной памяти.

------
Добавляем в таблицу новую точку, которая имеет свойства:

* Находится на координатах 4;5.5
* Окрашена в черный цвет
* Размер точки: 1

```python hl_lines="5 6 7"
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(
    position=Position(4, 5.5), color='black', size=1
)
points.insert(
    position=Position(5, 6.5), color='black', size=1
)
```

------
Добавляем в таблицу новую точку, которая имеет свойства:

* Находится на координатах 5;6.5
* Окрашена в черный цвет
* Размер точки: 1

```python hl_lines="8 9 10"
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(
    position=Position(4, 5.5), color='black', size=1
)
points.insert(
    position=Position(5, 6.5), color='black', size=1
)
```

### Код v2
Если сравнить эти точки, то можно заметить, 
что они одинакового размера и цвета.<br>
Можно ли сократить этот маленький код? — Да, можно.

Посмотрим на описание таблицы:
```python
class Points(Table):
    position: Position
    color: str = 'black'
    size: int = 1
```

Видим что поля `color` и `size` имеют значения по умолчанию. 
А теперь смотрим какими свойствами обладают точки, 
которые мы добавили, и осознаём что указали такие же значения.
Исходя из этого меняем наш код.

```python
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(position=Position(4, 5.5))
points.insert(position=Position(5, 6.5))
```

Этот код добавит в базу данных такие же точки, 
как и пред идущая версия кода.

!!! note "Интересный факт"
    Если бы мы указали значение по умолчанию для поля `position`,
    то мы могли бы создавать новые точки используя строку:
    ```python
    points.insert()
    ```

### Код v3
Давайте добавим больше различных точек.

```python
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(position=Position(4, 5.5))
points.insert(position=Position(5, 6.5))
points.insert(position=Position(0, 0), color='red')
points.insert(position=Position(-1, 1.2), size=2)
points.insert(position=Position(2, -2), color='blue', size=3)
```

!!! warning "Не забывайте"
    При добавлении записей в базу данных, 
    значения полей необходимо передавать того же типа, 
    что при описании таблицы
    ```python
    points.insert(position=Position(4, 5.5))
    ```