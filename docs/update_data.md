# Изменение данных
Как мы выяснили по умолчанию метод `filter` 
возвращает объекты класса, описывающего таблицу.<br>
Что же нам это даёт? — Мы можем обращаться к отдельным полям.<br>

```python hl_lines="11 12 13 14"
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(position=Position(4, 5.5))
points.insert(position=Position(5, 6.5))
points.insert(position=Position(0, 0), color='red')
points.insert(position=Position(-1, 1.2), size=2)
points.insert(position=Position(2, -2), color='blue', size=3)

obj = points.filter(id=1)
print(f'{obj.position=}')
print(f'{obj.color=}')
print(f'{obj.size=}')
```

!!! note ""
    Не выделенный код мы взяли из пред идущей темы. 
    Дальше мы будем опускать его.

------

Если мы можем к ним обращаться, 
давайте попробуем изменить их.

```python
obj = points.filter(id=1)
obj.color = 'green'
```

Изменить значение поля у нас получилось. 
Но в базе данных это изменение не сохраниться.

В Sqlite3 API есть 2 метода для изменений значений:
`save` и `update`. Давайте познакомимся с ними.

## Метод save()
Метод, сохраняющий изменения полей объекта в таблицу.<br>
Метод не принимает параметры.

```python
obj = points.filter(id=1)
obj.color = 'green'
obj.size += 5
obj.save()
```

После выполнения данного кода первая точка в таблице 
изменит значение полей `color` и `size`, 
так же все эти изменения сохраняться в базе данных.

Этот метод изменения данных можно применять, 
если необходимо изменять объект на протяжении долгого времени 
и только в конце сохранить оставшееся в итоге.

## Метод update()
Метод, изменяющий объект и сохраняющий эти изменения.<br>
В качестве параметров принимает поля, 
которые необходимо изменить, и их значения.

```python
obj = points.filter(id=1)
obj.update(color='green', size=obj.size + 5)
```

Данный код изменит объект так же, как и код, 
который приведён в разделе про метод `save`.

Этот метод подходит в тех случаях, 
когда у нас нет необходимости долгое время работать с объектом.
Например мы можем использовать его так: 
```python
points.filter(id=2).update(color='white')
```

## Пример
Поработаем с нашими точками.

```python
from data_base import Points, Position
points = Points(db_path=':memory:')
points.create_table()

points.insert(position=Position(4, 5.5))
points.insert(position=Position(5, 6.5))
points.insert(position=Position(0, 0), color='red')
points.insert(position=Position(-1, 1.2), size=2)
points.insert(position=Position(2, -2), color='blue', size=3)

first_point = points.filter(id=1)
first_point.position.x -= 4
first_point.position.y = 0

points.filter(id=2).update(size=first_point.size + 1)

third_point = points.filter(id=3)
third_point.update(size=first_point.size + 1, color='blue')
first_point.color = third_point.color

blue_points = points.filter(return_list=True, color='blue')
for point in blue_points:
    first_point.size += point.size

first_point.save()

# Посмотрим на изменения
for obj in points.filter():
    print(obj, '\n')
```

!!! warning "Не забывайте"
    При изменении полей, указывайте данные того же типа,
    что и при описании таблицы.
    ```python
    first_point = points.filter(id=1)
    first_point.position = Position(0, 0)
    first_point.save()
    ```
