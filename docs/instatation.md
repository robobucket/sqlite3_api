# Установка

Убедитесь, что у вас установлен Python 3.8+

```bash
python -V
```

------

Вы можете установить последнюю Sqlite3 API

=== "pip"
    
    ```bash
    python -m pip install sqlite3-api
    ```

=== "GitHub"

    ```bash
    python -m pip install git+https://github.com/AlexDev-py/sqlite3_api
    ```

------

Проверьте, чтобы библиотека установилась корректно:

```bash
python -m pip show sqlite3-api
```