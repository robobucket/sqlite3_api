# Введение

Перед тем как начать работу с базой данных, 
необходимо создать классы, описывающие таблицы.<br>
В этом разделе мы узнаем всё об их создании.

## Points

Напишем небольшой класс, который описывает то, 
как мы будем хранить некоторые данные 
о точках на двумерной плоскости.

```python
from sqlite3_api import Table


class Points(Table):
    x_cord: int
    y_cord: int
    color: str
    size: int
```

Этот код мы будем модифицировать по мере изучения материала.<br>
А пока что детально разберём этот пример.


## Разбор примера
Импортируем `Table` — класс, связывающий базу данных и объекты.

```python hl_lines="1"
from sqlite3_api import Table


class Points(Table):
    x_cord: int
    y_cord: int
    color: str
    size: int
```

-----
Создаем класс, называем его так, как хотели бы, что бы он хранился в базе данных.
<br>Наш класс должен наследовать весь функционал от ранее импортированного `Table`, 
для дальнейшей работы с базой данных.

```python hl_lines="4"
from sqlite3_api import Table


class Points(Table):
    x_cord: int
    y_cord: int
    color: str
    size: int
```

-----
Описываем названия полей и их типы данных.

```python hl_lines="5 6 7 8"
from sqlite3_api import Table


class Points(Table):
    x_cord: int
    y_cord: int
    color: str
    size: int
```

!!! note ""
    Тип данных, хранящийся в поле, указывается в аннотации.<br>
    Это сделано для лёгкого указания значений по умолчанию.

-----
