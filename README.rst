Install from `PyPI <https://pypi.org/project/sqlite3-api>`_::

    pip install sqlite3-api


Install from `GitHub <https://github.com/AlexDev-py/sqlite3_api.git>`_::

    git clone https://github.com/AlexDev-py/sqlite3_api.git


More information in the
`GitHub page <https://github.com/AlexDev-py/sqlite3_api/tree/2.0.0>`_